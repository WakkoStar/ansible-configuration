
# Initialize servers

ansible-playbook -i inventory.ini init_conf.yml --ask-become-pass --ask-vault-pass

# Deploy frontend

ansible-playbook -i inventory.ini deploy_frontend.yml --ask-become-pass --ask-vault-pass

# Deploy backend

ansible-playbook -i inventory.ini deploy_backend.yml --ask-become-pass --ask-vault-pass
